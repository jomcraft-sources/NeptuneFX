package net.jomcraft.neptunefx;

import java.io.File;

import net.jomcraft.neptunefx.font.FontRendererClass;
import net.minecraft.client.Minecraft;

public class ClientFontHandler {
	
	public static FontRendererClass fontRenderer;
	public static final Minecraft MC = Minecraft.getInstance();
	public static final File mcDataDir = MC.gameDir;
	
	public static void regFont() {
		fontRenderer = new FontRendererClass();
		fontRenderer.readFontTexture();
	}

}
