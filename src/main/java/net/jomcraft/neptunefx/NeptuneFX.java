package net.jomcraft.neptunefx;

import java.io.InputStream;
import java.util.ArrayList;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.electronwill.nightconfig.core.CommentedConfig;
import com.electronwill.nightconfig.toml.TomlParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ExtensionPoint;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(value = NeptuneFX.MODID)
public class NeptuneFX {

	public static final String MODID = "neptunefx";
	public static final String VERSION = getModVersion();
	public static final Logger log = LogManager.getLogger(NeptuneFX.MODID);
	public static boolean setUp = false;
	public static NeptuneFX instance;
	public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	public static boolean isEnabled = false;
	public static int targetMS = 9;
	
	@SuppressWarnings("deprecation")
	public NeptuneFX() {
		instance = this;
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
			
			if (setUp)
				return;
			
			FMLJavaModLoadingContext.get().getModEventBus().addListener(this::postInit);
			
			setUp = true;
			MinecraftForge.EVENT_BUS.register(NeptuneFX.class);
			ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.DISPLAYTEST, () -> Pair.of(()-> "ANY", (remote, isServer) -> true));
		});
		
		DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> () -> {
			NeptuneFX.log.log(Level.WARN, "DefaultSettings is a client-side mod only! It won't do anything on servers!");
		});

	}
	
	@SuppressWarnings("unchecked")
	public static String getModVersion() {
		//Stupid FG 3 workaround
		TomlParser parser = new TomlParser();
		InputStream stream = NeptuneFX.class.getClassLoader().getResourceAsStream("META-INF/mods.toml");
		CommentedConfig file = parser.parse(stream);

		return ((ArrayList<CommentedConfig>) file.get("mods")).get(0).get("version");
	}
	
	@SuppressWarnings("deprecation")
	public void postInit(FMLLoadCompleteEvent event) {
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
			if(getEnabled())
				ClientFontHandler.regFont();

		});
		
		DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> () -> {
			NeptuneFX.log.log(Level.WARN, "DefaultSettings is a client-side mod only! It won't do anything on servers!");
		});
	}

	
	public static NeptuneFX getInstance() {
		return instance;
	}
	
	public static boolean getEnabled() {
		return isEnabled;
	}
	
	public static void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}
}
